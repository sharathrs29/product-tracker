package com.productTracker.productTracker.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.productTracker.productTracker.model.Order;
import com.productTracker.productTracker.model.Product;
import com.productTracker.productTracker.service.ProductTrackerService;

@RestController
public class ProductTrackerController {

	@Autowired
	private ProductTrackerService productTrackerService;

	@GetMapping(path = "/get-products")
	public ResponseEntity<List<Product>> getProducts() {
		List<Product> products = productTrackerService.getProducts();

		return ResponseEntity.ok(products);
	}

	@PostMapping(path = "/insert-product")
	public ResponseEntity<Object> insertProduct(@RequestBody Product product) {
		productTrackerService.insertProduct(product);
		return ResponseEntity.ok("Success");
	}

	@GetMapping(path = "/get-order-by-id")
	public ResponseEntity<Map<String, Integer>> getOrderById(@RequestParam Integer productId) {
		Map<String, Integer> orders = productTrackerService.getOrderByProductId(productId);

		return ResponseEntity.ok(orders);
	}

	@PostMapping(path = "/insert-order")
	public ResponseEntity<Object> insertOrder(@RequestBody Order order) {
		productTrackerService.insertOrder(order);
		return ResponseEntity.ok("Success");
	}
}
