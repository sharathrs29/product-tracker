package com.productTracker.productTracker.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.productTracker.productTracker.dao.OrderInformationDao;
import com.productTracker.productTracker.dao.ProductInformationDao;
import com.productTracker.productTracker.entity.OrderInformation;
import com.productTracker.productTracker.entity.ProductInformation;
import com.productTracker.productTracker.model.Order;
import com.productTracker.productTracker.model.Product;
import com.productTracker.productTracker.service.ProductTrackerService;

@Service
public class ProductTrackerServiceImpl implements ProductTrackerService {

	@Autowired
	private ProductInformationDao productTrackerDao;

	@Autowired
	private OrderInformationDao orderInformationDao;

	@Override
	public void insertProduct(Product product) {
		ProductInformation productInformation = new ProductInformation();
		productInformation.setProductName(product.getProductName());
		productInformation.setProductDescription(product.getProductDescription());
		productInformation.setCreatedAt(new Date());
		productTrackerDao.save(productInformation);

	}

	@Override
	public List<Product> getProducts() {
		Iterable<ProductInformation> productInformation = productTrackerDao.findAll();
		List<Product> products = new ArrayList<>();
		productInformation.forEach(e -> {
			Product product = new Product();
			product.setProductId(e.getId());
			product.setCreatedAt(e.getCreatedAt());
			product.setModifiedAt(e.getModifiedAt());
			product.setProductDescription(e.getProductDescription());
			product.setProductName(e.getProductName());
			products.add(product);
		});
		return products;

	}

	@Override
	public Map<String, Integer> getOrderByProductId(Integer productId) {
		List<OrderInformation> orderInformations = orderInformationDao.findByProductId(String.valueOf(productId));
		Map<String, Integer> productDate = new HashMap<>();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM yy");
		orderInformations.forEach(e -> {
			Integer orderCount = 1;
			if (productDate.containsKey(sdf.format(e.getOrderDate()))) {
				orderCount = productDate.get(sdf.format(e.getOrderDate()));
				orderCount++;
			}
			productDate.put(sdf.format(e.getOrderDate()), orderCount);
		});
		return productDate;
	}

	@Override
	public void insertOrder(Order order) {
		OrderInformation orderInformation = new OrderInformation();
		orderInformation.setCreatedAt(new Date());
		Calendar cal = Calendar.getInstance(); 
		cal.add(Calendar.MONTH, 1);
		orderInformation.setOrderDate(cal.getTime());
		orderInformation.setProductId(order.getProductId());
		orderInformationDao.save(orderInformation);

	}

}
