package com.productTracker.productTracker.entity;

import java.util.Date;

import org.hibernate.annotations.Table;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Table(appliesTo = "product_information")
@Data
public class ProductInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name="product_name")
	private String productName;

	@Column(name="product_description")
	private String productDescription;

	@Column(name="created_at")
	private Date createdAt;

	@Column(name="modified_at")
	private Date modifiedAt;
}
