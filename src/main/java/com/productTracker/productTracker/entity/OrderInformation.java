package com.productTracker.productTracker.entity;

import java.util.Date;

import org.hibernate.annotations.Table;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Table(appliesTo = "order_information")
@Data
public class OrderInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="product_id")
	private String productId;

	@Column(name="order_date")
	private Date orderDate;
	
	@Column(name="created_at")
	private Date createdAt;

	@Column(name="modified_at")
	private Date modifiedAt;
}
