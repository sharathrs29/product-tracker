package com.productTracker.productTracker.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.productTracker.productTracker.entity.ProductInformation;

@Repository
public interface ProductInformationDao extends CrudRepository<ProductInformation, Integer> {

}
