package com.productTracker.productTracker.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.productTracker.productTracker.entity.OrderInformation;

@Repository
public interface OrderInformationDao extends CrudRepository<OrderInformation, Integer>{
	
	@Query("SELECT o FROM OrderInformation o WHERE o.productId = :productId")
	 public List<OrderInformation> findByProductId(@Param("productId") String productId);

}
