package com.productTracker.productTracker.model;

import lombok.Data;

@Data
public class OrderDetails {

	private String orderCount;

	private String orderDate;
}
