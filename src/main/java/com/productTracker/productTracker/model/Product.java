package com.productTracker.productTracker.model;

import java.util.Date;

import lombok.Data;

@Data
public class Product {

	private int productId;
	private String productName;
	private String productDescription;
	private Date createdAt;
	private Date modifiedAt;

}
