package com.productTracker.productTracker.model;

import java.util.Date;

import lombok.Data;

@Data
public class Order {
	
	private int orderId;

	private String productId;

	private Date orderDate;

	private Date createdAt;

	private Date modifiedAt;
}
