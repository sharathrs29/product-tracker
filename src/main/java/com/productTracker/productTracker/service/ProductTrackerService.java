package com.productTracker.productTracker.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.productTracker.productTracker.model.Order;
import com.productTracker.productTracker.model.Product;

@Service
public interface ProductTrackerService {

	void insertProduct(Product product);

	List<Product> getProducts();

	Map<String, Integer> getOrderByProductId(Integer productId);

	void insertOrder(Order order);

}
